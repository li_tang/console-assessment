/**
 * Example problem with existing solution and passing test.
 * See problem 0 in the spec file for the assertion
 * @returns {string}
 */
exports.example = () => 'hello world';

exports.stripPrivateProperties = (excludeProps,inputArr) => {
    return inputArr.map((e)=>{
        excludeProps.forEach(element => {
            //due to requirement that mutation is fine here, direct modifications
            //are used to mutate object
            if(e.hasOwnProperty(element)){
                delete e[element];
            }
        });
        return e;
    });
};
exports.excludeByProperty = (filterProp,inputArr) => {
    return inputArr.filter(e=>(!e[filterProp]));
};
exports.sumDeep = (inputArr) => {
    return inputArr.map((e)=>{
        return Object.assign({},e,{
            objects:e.objects.map(e=>e.val).reduce((acc,cur)=>acc+cur,0)
        });
    });
};
exports.applyStatusColor = (colorCodes,statusList) => {
    return statusList
    .filter(status=>Object.keys(colorCodes).find(color=>colorCodes[color].includes(status.status)))
    .map(status=>Object.assign({},status,{
            color:Object.keys(colorCodes).find(color=>colorCodes[color].includes(status.status))
        }));
};
exports.createGreeting = (greetingFn,greetingMsg) => {
    return name => greetingFn(greetingMsg,name);
    
};
exports.setDefaults = (defaultOpt) => {
    return (userObj)=>{
        return Object.keys(defaultOpt).reduce((accObj,prop)=>{
            if(!userObj.hasOwnProperty(prop)){
                accObj[prop] = defaultOpt[prop];
            }  
            return accObj;
        },Object.assign({},userObj));
    }
};
exports.fetchUserByNameAndUsersCompany = (userName,services) => {
    return Promise.all([
        services.fetchUsers()
        .then(users=>users.find(user=>user.name === userName))
        .then(user=>services.fetchCompanyById(user.companyId)
        .then(company=>{
            return {
                company:company,
                user:user
            };
        })),
        services.fetchStatus()
    ]).then(res=>{
        return {
            company:res[0].company,
            status:res[1],
            user:res[0].user
        };
    });
};
